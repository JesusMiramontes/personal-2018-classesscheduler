﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesScheduler
{
    class Clase
    {
        private string _id;
        private string _name;
        private int _semester;
        private int _semester_real;
        private bool _taken;
        private int _grade;
        private List<Day> _week_days;
        private string _teacher;

        public string teacher
        {
            get { return _teacher; }
            set { _teacher = value; }
        }

        public Clase(string name, string id, List<Day> week_days, string teacher)
        {
            this.name = name;
            this.id = id;
            this.week_days = week_days;
            this.teacher = teacher;
            this.Taken = false;
        }

        public List<Day> week_days
        {
            get { return _week_days; }
            set { _week_days = value; }
        }


        public int grade
        {
            get { return _grade; }
            set { _grade = value; }
        }


        public bool Taken
        {
            get { return _taken; }
            set { _taken = value; }
        }


        public int semester_real
        {
            get { return _semester_real; }
            set { _semester_real = value; }
        }


        public int semester
        {
            get { return _semester; }
            set { _semester = value; }
        }


        public string name
        {
            get { return _name; }
            set { _name = value; }
        }


        public string id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string short_id
        {
            get { return id.Substring(0, id.Length - 1);  }
        }

    }
}
