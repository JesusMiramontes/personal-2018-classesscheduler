﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesScheduler
{
    class Analyzer
    {
        private List<Day> _days;
        private List<string> times = new List<string>();
        private Dictionary<string, List<Day>> horarios = new Dictionary<string, List<Day>>();

        public Analyzer(List<Day> days)
        {
            this.days = days;

        }

        public List<Day> days
        {
            get { return _days; }
            set { _days = value; }
        }

        public void bruteForce()
        {
            splitByHours();

        }

        private void splitByHours()
        {
            foreach (Day day in days)
            {
                if (!horarios.ContainsKey(day.inicio_TwentyFourHourFormat))
                {
                    horarios.Add(day.inicio_TwentyFourHourFormat, new List<Day>());
                }
                horarios[day.inicio_TwentyFourHourFormat].Add(day);
            }
        }
    }
}
