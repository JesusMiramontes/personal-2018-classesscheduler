﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace ClassesScheduler
{
    class Time
    {
        private int _id;
        private DateTime _begin;
        private DateTime _end;
        private string _classroom;
        private Dictionary<string, bool> _week_days = new Dictionary<string, bool>();


        public string classroom
        {
            get { return _classroom; }
            set { _classroom = value; }
        }


        public int id
        {
            get { return _id; }
            set { _id = value; }
        }


        public DateTime end
        {
            get { return _end; }
            set { _end = value; }
        }


        public DateTime begin
        {
            get { return _begin; }
            set { _begin = value; }
        }

        private void initWeekDays()
        {
            bool f = false;
            this._week_days.Add("Lunes",f);
            this._week_days.Add("Martes", f);
            this._week_days.Add("Miercoles", f);
            this._week_days.Add("Jueves", f);
            this._week_days.Add("Viernes", f);
        }

        private void setWeekDayValue(int pos, bool val)
        {
            this._week_days[getWeekDayKeyByIndex(pos)] = val;
        }

        private string getWeekDayKeyByIndex(int index)
        {
            switch (index)
            {
                case 0:
                    return "Lunes";
                    break;
                case 1:
                    return "Martes";
                    break;
                case 2:
                    return "Miercoles";
                    break;
                case 3:
                    return "Jueves";
                    break;
                case 4:
                    return "Viernes";
                    break;
                default:
                    return "nil";
                    break;
            }
        }

        public void bulkSetWeekdays(string s)
        {
            foreach(char c in s)
            {
                setWeekDayValue(getWeekDayIndexByLetter(c),true);
            }
        }

        private int getWeekDayIndexByLetter(char c)
        {
            switch (c)
            {
                case 'L':
                    return 0;
                case 'M':
                    return 1;
                case 'X':
                    return 2;
                case 'J':
                    return 3;
                case 'V':
                    return 4;
                default: return -1;
            }
        }

    }
}
