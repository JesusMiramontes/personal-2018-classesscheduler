﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassesScheduler
{
    public partial class Form1 : Form
    {
        Formatter formatter = new Formatter();
        List<Clase> clases = new List<Clase>();
        private Horario_deseado hd;
        private List<Clase> no_tomadas;
        private List<Day> horarios_tentativos = new List<Day>();

        public Form1()
        {
            InitializeComponent();
        }

        private void btn_copy_clipboard_Click(object sender, EventArgs e)
        {
            textBox_input.Text = Clipboard.GetText();
        }

        private void btn_format_Click(object sender, EventArgs e)
        {
            formatter.htmlToClasses(textBox_input.Text, clases);
        }

        private void btn_taken_Click(object sender, EventArgs e)
        {
            string s = textBox_input.Text;

            s = s.Replace("\r", "\n");
            s = s.Replace("\n\n", "#");
            string[] ss = s.Split('#');

            for (int i = 0; i < ss.Length - 1; i++)
            {
                if (ss[i] != "A16")
                {
                    foreach (Clase clase in clases.FindAll(x => x.short_id == ss[i]))
                    {
                        clase.Taken = true;

                    }
                }
            }

            no_tomadas = clases.FindAll(z => z.Taken == false);

            dataGridView1.DataSource = no_tomadas;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void btn_horario_deseado_Click(object sender, EventArgs e)
        {
            hd = new Horario_deseado(Int32.Parse(input_clases.Text), DateTime.Parse(time_inicio.Text), DateTime.Parse(time_fin.Text));
        }

        private void find_Click(object sender, EventArgs e)
        {
            horarios_tentativos = new List<Day>();
            foreach (Clase clase in no_tomadas)
            {
                foreach (Day day in clase.week_days)
                {
                    horarios_tentativos.Add(day);
                }
            }

            horarios_tentativos =
                horarios_tentativos.FindAll(x => x.inicio.Hour >= hd.inicio.Hour && x.fin.Hour <= hd.fin.Hour);
            horarios_tentativos = horarios_tentativos.OrderBy(x => x.inicio_TwentyFourHourFormat).ToList();
            dgv_horarios.DataSource = horarios_tentativos;

            Analyzer analyzer = new Analyzer(horarios_tentativos);

            analyzer.bruteForce();
        }

        private void btn_display_classes_Click(object sender, EventArgs e)
        {
            List<Clase> clases = new List<Clase>();
            foreach (Day day in horarios_tentativos)
            {
                if(!clases.Exists(x => x.id == day.class_id))
                    clases.Add(no_tomadas.Find(x => x.id == day.class_id));
            }

            dgv_horarios.DataSource = clases;

        }
    }
}
