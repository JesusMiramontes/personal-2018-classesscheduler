﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesScheduler
{
    class Day
    {
        private static string[] week_days = {"Lunes", "Martes", "Miercoles", "Juever", "Viernes", "Sabado", "Domingo"};

        private DateTime _inicio;
        private DateTime _fin;
        private string _classroom;
        private int _day;
        private string _class_id;

        public string class_id
        {
            get { return _class_id; }
            set { _class_id = value; }
        }


        public Day(int day, string classroom, DateTime inicio, DateTime fin, string class_id)
        {
            this.day_index = day;
            this.classroom = classroom;
            this.inicio = inicio;
            this.fin = fin;
            this.class_id = class_id;
        }

        public int day_index
        {
            get { return _day; }
            set { _day = value; }
        }

        public string day
        {
            get { return week_days[_day]; }
        }

        public string classroom
        {
            get { return _classroom; }
            set { _classroom = value; }
        }

        public DateTime fin
        {
            get { return _fin; }
            set { _fin = value; }
        }

        public DateTime inicio
        {
            get { return _inicio; }
            set { _inicio = value; }
        }

        public string inicio_TwentyFourHourFormat
        {
            get{ return inicio.ToString("HH:mm"); }
        }

        public string fin_TwentyFourHourFormat
        {
            get { return fin.ToString("HH:mm"); }
        }

    }
}
