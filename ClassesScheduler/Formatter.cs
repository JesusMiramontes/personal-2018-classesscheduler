﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using HtmlAgilityPack;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace ClassesScheduler
{
    class Formatter
    {
        public static string[] keywords = {"table", "tr", "td"};

        public void getTable(string s)
        {
            string[] t = s.Split("<table".ToCharArray());
        }

        public void htmlToClasses(string s, List<Clase> clases)
        {
            HtmlDocument html_doc = new HtmlDocument();
            html_doc.LoadHtml(s);
            var children = html_doc.DocumentNode.ChildNodes;
            var tds = html_doc.DocumentNode.SelectSingleNode("//table").SelectNodes("//td");

            for (int i = 8; i < tds.Count;)
            {
                string materia = cleanString(tds[i++].InnerText);
                string grupo = cleanString(tds[i++].InnerText);
                string[] days = new string[5];
                for (int j = 0; j < days.Length; j++)
                {
                    days[j] = cleanString(tds[i++].InnerText);
                }
                string catedratico = cleanString(tds[i++].InnerText);

                List<Day> week_days = new List<Day>();

                for (int k = 0; k < days.Length; k++)
                {
                    if (!days[k].StartsWith("0"))
                    {
                        DateTime[] inicio_fin = stringToDateTime(splitHourAndClassroom(days[k]));
                        Day d = new Day(k,splitHourAndClassroom(days[k],'C'),inicio_fin[0], inicio_fin[1], grupo);
                        week_days.Add(d);
                    }
                }

                clases.Add(new Clase(materia, grupo, week_days, catedratico));
            }
        }

        private string cleanString(string s)
        {
            string output = s.Replace("\r\n", "");
            int end_at_char = 0;
            bool continuar = true;
            for (int i = output.Length-1; i > 0 && continuar; i--)
            {
                if (output[i].Equals(' '))
                {
                    end_at_char = i;
                }
                else
                {
                    continuar = false;
                }
            }

            if (end_at_char > 0)
                output = output.Substring(0, end_at_char);

            return output;
        }

        private DateTime[] stringToDateTime(string s)
        {
            DateTime[] output = new DateTime[2];
            int split_by = 2;

            if (s.Length == 3)
            {
                split_by = 1;
            }

            DateTime date_time = new DateTime();
            output[0] = date_time.AddHours(Int32.Parse(s.Substring(0,split_by)));
            date_time = new DateTime();
            output[1] = date_time.AddHours(Int32.Parse(s.Substring(split_by)));

            return output;
        }

        private string splitHourAndClassroom(string s, char hc = 'H')
        {
            int i = (hc == 'H') ? 0 : 1;
            return s.Split('/')[i];
        }
    }
}
