﻿namespace ClassesScheduler
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.btn_taken = new System.Windows.Forms.Button();
            this.btn_format = new System.Windows.Forms.Button();
            this.btn_copy_clipboard = new System.Windows.Forms.Button();
            this.textBox_input = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.input_clases = new System.Windows.Forms.TextBox();
            this.btn_horario_deseado = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.time_inicio = new System.Windows.Forms.DateTimePicker();
            this.time_fin = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.find = new System.Windows.Forms.Button();
            this.dgv_horarios = new System.Windows.Forms.DataGridView();
            this.btn_display_classes = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_horarios)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(702, 541);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(628, 515);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.btn_taken);
            this.tabPage2.Controls.Add(this.btn_format);
            this.tabPage2.Controls.Add(this.btn_copy_clipboard);
            this.tabPage2.Controls.Add(this.textBox_input);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(628, 515);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.Location = new System.Drawing.Point(3, 420);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(622, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btn_taken
            // 
            this.btn_taken.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_taken.Location = new System.Drawing.Point(3, 443);
            this.btn_taken.Name = "btn_taken";
            this.btn_taken.Size = new System.Drawing.Size(622, 23);
            this.btn_taken.TabIndex = 3;
            this.btn_taken.Text = "taken";
            this.btn_taken.UseVisualStyleBackColor = true;
            this.btn_taken.Click += new System.EventHandler(this.btn_taken_Click);
            // 
            // btn_format
            // 
            this.btn_format.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_format.Location = new System.Drawing.Point(3, 466);
            this.btn_format.Name = "btn_format";
            this.btn_format.Size = new System.Drawing.Size(622, 23);
            this.btn_format.TabIndex = 2;
            this.btn_format.Text = "Format";
            this.btn_format.UseVisualStyleBackColor = true;
            this.btn_format.Click += new System.EventHandler(this.btn_format_Click);
            // 
            // btn_copy_clipboard
            // 
            this.btn_copy_clipboard.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_copy_clipboard.Location = new System.Drawing.Point(3, 489);
            this.btn_copy_clipboard.Name = "btn_copy_clipboard";
            this.btn_copy_clipboard.Size = new System.Drawing.Size(622, 23);
            this.btn_copy_clipboard.TabIndex = 1;
            this.btn_copy_clipboard.Text = "Copy";
            this.btn_copy_clipboard.UseVisualStyleBackColor = true;
            this.btn_copy_clipboard.Click += new System.EventHandler(this.btn_copy_clipboard_Click);
            // 
            // textBox_input
            // 
            this.textBox_input.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_input.Location = new System.Drawing.Point(3, 3);
            this.textBox_input.Multiline = true;
            this.textBox_input.Name = "textBox_input";
            this.textBox_input.Size = new System.Drawing.Size(622, 509);
            this.textBox_input.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(628, 515);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 3);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(622, 509);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgv_horarios);
            this.tabPage4.Controls.Add(this.panel1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(694, 515);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "tabPage4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_display_classes);
            this.panel1.Controls.Add(this.find);
            this.panel1.Controls.Add(this.time_fin);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.time_inicio);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.input_clases);
            this.panel1.Controls.Add(this.btn_horario_deseado);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(688, 33);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clases";
            // 
            // input_clases
            // 
            this.input_clases.Location = new System.Drawing.Point(47, 4);
            this.input_clases.Name = "input_clases";
            this.input_clases.Size = new System.Drawing.Size(100, 20);
            this.input_clases.TabIndex = 2;
            this.input_clases.Text = "5";
            // 
            // btn_horario_deseado
            // 
            this.btn_horario_deseado.Location = new System.Drawing.Point(407, 2);
            this.btn_horario_deseado.Name = "btn_horario_deseado";
            this.btn_horario_deseado.Size = new System.Drawing.Size(75, 23);
            this.btn_horario_deseado.TabIndex = 3;
            this.btn_horario_deseado.Text = "h. deseado";
            this.btn_horario_deseado.UseVisualStyleBackColor = true;
            this.btn_horario_deseado.Click += new System.EventHandler(this.btn_horario_deseado_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(156, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "inicio";
            // 
            // time_inicio
            // 
            this.time_inicio.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.time_inicio.Location = new System.Drawing.Point(187, 4);
            this.time_inicio.Name = "time_inicio";
            this.time_inicio.Size = new System.Drawing.Size(86, 20);
            this.time_inicio.TabIndex = 6;
            this.time_inicio.Value = new System.DateTime(2018, 8, 9, 7, 0, 0, 0);
            // 
            // time_fin
            // 
            this.time_fin.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.time_fin.Location = new System.Drawing.Point(303, 4);
            this.time_fin.Name = "time_fin";
            this.time_fin.Size = new System.Drawing.Size(86, 20);
            this.time_fin.TabIndex = 8;
            this.time_fin.Value = new System.DateTime(2018, 8, 9, 8, 0, 0, 0);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(283, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "fin";
            // 
            // find
            // 
            this.find.Location = new System.Drawing.Point(489, 3);
            this.find.Name = "find";
            this.find.Size = new System.Drawing.Size(61, 23);
            this.find.TabIndex = 9;
            this.find.Text = "btn_find";
            this.find.UseVisualStyleBackColor = true;
            this.find.Click += new System.EventHandler(this.find_Click);
            // 
            // dgv_horarios
            // 
            this.dgv_horarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_horarios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_horarios.Location = new System.Drawing.Point(3, 36);
            this.dgv_horarios.Name = "dgv_horarios";
            this.dgv_horarios.Size = new System.Drawing.Size(688, 476);
            this.dgv_horarios.TabIndex = 1;
            // 
            // btn_display_classes
            // 
            this.btn_display_classes.Location = new System.Drawing.Point(556, 4);
            this.btn_display_classes.Name = "btn_display_classes";
            this.btn_display_classes.Size = new System.Drawing.Size(75, 23);
            this.btn_display_classes.TabIndex = 10;
            this.btn_display_classes.Text = "Classes";
            this.btn_display_classes.UseVisualStyleBackColor = true;
            this.btn_display_classes.Click += new System.EventHandler(this.btn_display_classes_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 541);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_horarios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_copy_clipboard;
        private System.Windows.Forms.TextBox textBox_input;
        private System.Windows.Forms.Button btn_format;
        private System.Windows.Forms.Button btn_taken;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DateTimePicker time_fin;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker time_inicio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox input_clases;
        private System.Windows.Forms.Button btn_horario_deseado;
        private System.Windows.Forms.Button find;
        private System.Windows.Forms.DataGridView dgv_horarios;
        private System.Windows.Forms.Button btn_display_classes;
    }
}

