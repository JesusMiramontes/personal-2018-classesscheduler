﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassesScheduler
{
    class Horario_deseado
    {
        private int _no_clases;
        private DateTime _inicio;
        private DateTime _fin;

        public Horario_deseado(int no_clases, DateTime inicio, DateTime fin)
        {
            this.no_clases = no_clases;
            this.inicio = inicio;
            this.fin = fin;
        }

        public DateTime fin
        {
            get { return _fin; }
            set { _fin = value; }
        }


        public DateTime inicio
        {
            get { return _inicio; }
            set { _inicio = value; }
        }


        public int no_clases
        {
            get { return _no_clases; }
            set { _no_clases = value; }
        }

    }
}
